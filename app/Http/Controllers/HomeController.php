<?php

namespace App\Http\Controllers;

use App\Order;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $view = view('home');

        //Persistencia da model, pegando uma order do banco com relacionamento
        //$orderId = $request->route()->parameters()['param'];
        //$orderModel = new Order();
        //$order = $orderModel->with(['products'])->findOrFail($orderId);

        //Enviar variavel para view
        //$viewData['teste'] = 'mama mia';

        //Pegar parametro da rota
        //$request->route()->parameters()

        //Pegar parametro do post
        //$request->input()

        //Pegar parametro da URL
        //$request->query()

        //Pega o usuário que esta logado
        //Auth::user()

        //HINT
        //Tudo que estiver dentro de attributes de uma model é acessado só chamando como objeto

        //Mensagem de retorno
        //Session::flash('success','Office successfully added.');

        $view->with('teste', 'Variavel Teste testuda');
        //$view->with('order', $order);
        return $view;
    }
}
