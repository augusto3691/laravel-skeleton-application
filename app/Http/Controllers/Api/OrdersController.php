<?php

namespace App\Http\Controllers\Api;

use App\Order;
use App\Http\Controllers\Controller;

class OrdersController extends Controller
{
    use \App\Http\Controllers\ApiControllerTrait;

    protected $model;
    protected $relationships = ['products'];

    public function __construct(Order $model)
    {
        $this->model = $model;
    }
}
