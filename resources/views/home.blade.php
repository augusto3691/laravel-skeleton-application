@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
                    {{ $teste or 'sem variavel teste' }}
                    <br>
                    <br>
                    @can('btn-google')
                        <button class="btn btn-primary">Google</button>
                    @endcan
                </div>
            </div>
        </div>
    </div>
@endsection
