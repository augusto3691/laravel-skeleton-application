<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="Xenon Boostrap Admin Panel"/>
    <meta name="author" content=""/>

    <title>Login</title>

    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic">
    <link rel="stylesheet" href="{{asset('css/fonts/linecons/css/linecons.css')}}">
    <link rel="stylesheet" href="{{asset('css/fonts/fontawesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/xenon-core.css')}}">
    <link rel="stylesheet" href="{{asset('css/xenon-forms.css')}}">
    <link rel="stylesheet" href="{{asset('css/xenon-components.css')}}">
    <link rel="stylesheet" href="{{asset('css/xenon-skins.css')}}">
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">

    <script src="{{asset('js/jquery-1.11.1.min.js')}}"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script type="text/javascript">
        $(document).ready(function ($) {
            // Reveal Login form
            setTimeout(function () {
                $(".fade-in-effect").addClass('in');
            }, 1);
        });
    </script>


</head>
<body class="page-body login-page">
<div class="login-container">
    <div class="row">
        <div class="col-sm-6">
            <!-- Errors container -->
            <div class="errors-container">

            </div>
            <!-- Add class "fade-in-effect" for login form effect -->
            <form method="post" role="form" id="login" class="login-form fade-in-effect" action="{{ route('login') }}">
                {{ csrf_field() }}
                <div class="login-header">
                    <a href="#" class="logo">
                        <span>Login</span>
                    </a>
                </div>
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input id="email" type="email" class="form-control input-dark" placeholder="E-mail" name="email"
                           value="{{ old('email') }}" required autofocus>
                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input id="password" type="password" class="form-control input-dark" placeholder="senha"
                           name="password" required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
                {{--<div class="form-group">--}}
                {{--<div class="checkbox pull-right">--}}
                {{--<label>--}}
                {{--<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>--}}
                {{--Remember Me--}}
                {{--</label>--}}
                {{--</div>--}}
                {{--</div>--}}
                <div class="form-group">
                    <button type="submit" class="btn btn-dark btn-block text-left">
                        <i class="fa-lock"></i>
                        Log In
                    </button>
                </div>
                <div class="login-footer">
                    <a href="{{ route('password.request') }}">Esqueceu sua senha?</a>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Bottom Scripts -->
<script src="{{asset('js/bootstrap.min.js') }}"></script>
<script src="{{asset('js/TweenMax.min.js')}}"></script>
<script src="{{asset('js/resizeable.js')}}"></script>
<script src="{{asset('js/joinable.js')}}"></script>
<script src="{{asset('js/xenon-api.js')}}"></script>
<script src="{{asset('js/xenon-toggles.js')}}"></script>
<script src="{{asset('js/jquery-validate/jquery.validate.min.js')}}"></script>
<script src="{{asset('js/toastr/toastr.min.js')}}"></script>


<!-- JavaScripts initializations and stuff -->
<script src="{{asset('js/xenon-custom.js')}}"></script>

</body>
</html>