<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


\Illuminate\Support\Facades\Route::group(['middleware' => ['auth:api']], function () {

    \Illuminate\Support\Facades\Route::get('/user', function (Request $request) {
        return $request->user();
    })->middleware('auth:api');

    \Illuminate\Support\Facades\Route::resource('/banks', 'Api\BanksController');
    \Illuminate\Support\Facades\Route::resource('/accounts', 'Api\AccountsController');
    \Illuminate\Support\Facades\Route::resource('/orders', 'Api\OrdersController');
});
