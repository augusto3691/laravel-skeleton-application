<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {
            \Illuminate\Support\Facades\DB::table('users')->insert([
                'name' => 'Admin',
                'email' => 'admin@yopmail.com',
                'password' => bcrypt('w64c74h41'),
            ]);
            \Illuminate\Support\Facades\DB::table('users')->insert([
                'name' => 'Operator',
                'email' => 'operator@yopmail.com',
                'password' => bcrypt('w64c74h41'),
            ]);
        }
    }
}
